1. Install all packages

```shell
composer install
```

2. Change link of partners images to own in: 

```shell
assets/images/partners/
```

3. Add them in script

```shell
assets/scripts/main.js

let images = [
    "http://<domain>/assets/images/partners/andrewex.png",
    "http://<domain>/assets/images/partners/crossfit.jpg",
    "http://<domain>/assets/images/partners/falubaz-logo.png",
    "http://<domain>/assets/images/partners/fast.jpg",
    "http://<domain>/assets/images/partners/logo-scandicsofa.png",
    "http://<domain>/assets/images/partners/minibasket-logo.png",
    "http://<domain>/assets/images/partners/pkm.png",
    "http://<domain>/assets/images/partners/puszman-logo.png",
    "http://<domain>/assets/images/partners/rockwool-logo.png"
];
```

4. Change email information to your in:

```shell
scripts/Config.php
```
