"use strict";

let activitySubtitle = $("#activity-subtitle");
let activityText = $("#activity-text");

let firstActivity = $("#b2b");

let firstImage = $("#firstImage");
let secondImage = $("#secondImage");
let thirdImage = $("#thirdImage");

/**
 * Change to your images
 *
 * @type {string[]}
 */
let images = [
    // window.location.href - nie zmieniać, ma być przed każdym plikiem.
    // window.location.href generuje URL strony, na której znajduje się script.

    // window.location.href + "/assets/images/partners/andrewex.png",
    // window.location.href + "/assets/images/partners/rockwool-logo.png",
    // window.location.href + "/assets/images/partners/logo-scandicsofa.png",
    // window.location.href + "/assets/images/partners/crossfit.jpg",
    // window.location.href + "/assets/images/partners/falubaz-logo.png",
    // window.location.href + "/assets/images/partners/fast.jpg",
    // window.location.href + "/assets/images/partners/minibasket-logo.png",
    // window.location.href + "/assets/images/partners/pkm.png",
    // window.location.href + "/assets/images/partners/puszman-logo.png",

    // Zewnętrzne pliki mają posiadać całą ścieżkę do zdęcia, np:
    // "https://test.com/image.png",
	"https://soltix.pl/wp-content/uploads/2021/07/rw-soltix.png",
	"https://soltix.pl/wp-content/uploads/2021/07/a-soltix.png",
	"https://soltix.pl/wp-content/uploads/2021/07/ss-soltix.png",
	"https://soltix.pl/wp-content/uploads/2021/07/fast-soltix.png",
	"https://soltix.pl/wp-content/uploads/2021/07/cf-soltix.png",
	"https://soltix.pl/wp-content/uploads/2021/07/pusz-soltix.png",
	"https://soltix.pl/wp-content/uploads/2021/07/f-soltix.png",
	"https://soltix.pl/wp-content/uploads/2021/07/pkm-soltix.png",
	"https://soltix.pl/wp-content/uploads/2021/07/rw-soltix.png",
];

let index = 0;

setInterval(() => {
    if (index + 2 >= images.length) {
        index = 0;
    }

    firstImage.attr("src", images[index]);
    secondImage.attr("src", images[index + 1]);
    thirdImage.attr("src", images[index + 2]);

    index += 2;
}, 2500);

function changeText(object) {
    if (firstActivity.hasClass("active")) {
        firstActivity.removeClass("active");
    }

    if ($(window).width() > 1310) {
        $(object)
            .on('mouseenter', () => {
                activitySubtitle.text($(object).find("span[class=\"d-none\"]").text());
                activityText.text($(object).find("p[class=\"d-none\"]").text());
            })
            .on('mouseleave', () => {
                defaultText();
            });
    } else {
        $(object)
            .on('click', () => {
                activitySubtitle.text($(object).find("span[class=\"d-none\"]").text());
                activityText.text($(object).find("p[class=\"d-none\"]").text());

                if ($(window).width() <= 506) {
                    let loc = document.location.toString().split('#')[0];
                    document.location = loc + '#activity-left';
                    return false;
                }
            });
    }
}

function defaultText() {
    if (!firstActivity.hasClass("active")) {
        activitySubtitle.text(firstActivity.find("span[class=\"d-none\"]").text());
        activityText.text(firstActivity.find("p[class=\"d-none\"]").text());

        firstActivity.addClass("active");
    }
}

// function initMap() {
//     // The location
//     const place = { lat: 51.93500626330654, lng: 15.506009369588927 };
//
//     // The map, centered at location
//     const map = new google.maps.Map(
//         document.getElementById("map"),
//         {
//             zoom: 15,
//             center: place
//         }
//     )
//
//     new google.maps.Marker({
//         position: place,
//         map,
//     });
// }

let myCarousel = document.querySelector("#carousel");
let carousel = new bootstrap.Carousel(myCarousel, {
    interval: 10000,
});

$("#customFile").change(() => {
    let file = $("#customFile")[0].files[0].name;
    let fileLabel = $("#fileLabel");
    fileLabel.text(file);
});

function showMessage() {
	alert("Otrzymaliśmy Twoje dane kontaktowe. Skontaktujemy się jak tylko będziemy mieli możliwość.");
}
