<?php

use PHPMailer\PHPMailer\PHPMailer;

require "./vendor/autoload.php";
require 'scripts/Config.php';

$fullName = $_POST['fullName'];
$email = $_POST['email'];
$phoneNumber = $_POST['phoneNumber'];

$mail = new PHPMailer();

$body = "";

if (!empty($fullName) && !empty($email) && !empty($phoneNumber)) {
    $body = "Imię i nazwisko: ${fullName}<br />Email: ${email}<br />Numer telefonu: ${phoneNumber}";

    if (!empty($_FILES["myfilename"]["name"])) {
        $source = $_FILES['myfilename']['tmp_name'];
        $filename = $_FILES['myfilename']['name'];

        $mail->AddAttachment($source, $filename);
    }
} elseif (!empty($phoneNumber)) {
    $body = "Numer telefonu: ${phoneNumber}";
}

if (Config::$emailSMTPAuth && !empty($body)) {
    $mail->isSMTP();
    $mail->SMTPAuth = Config::$emailSMTPAuth;
    $mail->Port = Config::$emailPort;
    $mail->Host = Config::$emailHost;
    $mail->Username = Config::$emailUsername;
    $mail->Password = Config::$emailPassword;

    $mail->SMTPSecure = Config::$emailSMTPSecure;

    $mail->isSendmail();

    $mail->From = Config::$emailUsername;
    $mail->FromName = "Soltix Mail";

    $mail->addAddress('soltix@soltix.pl', "Soltix Mail");
    $mail->addCC('wojciech.smykowski@googlemail.com', 'Soltix Mail');

    $mail->Subject = "Nowy kontakt - Soltix Mail";

    $mail->CharSet = 'UTF-8';
    $mail->Encoding = 'base64';

    $mail->msgHTML($body);

    $mail->isHTML(true);

    if($mail->send()) {
	header("Location: /");
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Soltix</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="./assets/styles/bootstrap.min.css">

    <!-- Custom styles -->
    <link rel="stylesheet" href="./assets/styles/app.css">
    <!-- Media Queries -->
    <link rel="stylesheet" href="./assets/styles/media.css">

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon-16x16.png">
    <link rel="manifest" href="assets/site.webmanifest">
    <link rel="mask-icon" href="assets/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
</head>
<body>

<header id="app-header">
    <div class="overflow-hidden">
        <img src="assets/images/header-image.png" alt="Logo" class="header-image">
    </div>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light bg-transparent justify-content-between">
            <a class="navbar-brand" href="#">
                <img src="./assets/images/logo.svg" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse flex-grow-0" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="#stand-out">Dlaczego my</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#activity">Obszary działań</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#realization">Realizacje</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#app-footer">Kontakt</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="container">
        <h2 class="header-title"><span>Tworzymy</span><br>OPROGRAMOWANIE<br>dedykowane</h2>
        <div class="header-subtitle d-flex align-items-center">
            <img src="./assets/images/mini-logo.svg" alt="">
            <p class="m-0">Zmieniamy analogowe potrzeby na cyfrowe rozwiązania</p>
        </div>
        <form class="header-form position-relative" action="index.php" method="post" id="phone_form" onsubmit="showMessage()">
            <div class="form-group">
                <label for="phoneNumber" class="d-none">Numer telefonu</label>
                <input type="tel" pattern=".{9}" class="form-control border-0" id="phoneNumber"
                       placeholder="Numer telefonu" maxlength="9" name="phoneNumber" required>
            </div>
            <button type="submit" class="btn btn-warning">Zostaw kontakt</button>
        </form>
        <div class="d-flex justify-content-between header-bottom">
            <div class="left">
                <span>Zaufali nam:</span>
                <img src="" alt="" id="firstImage">
                <img src="" alt="" id="secondImage">
                <img src="" alt="" id="thirdImage">
            </div>
            <div class="right d-flex flex-column m-0 p-0 justify-content-center">
                <div class="d-flex justify-content-end">
                    <a href="mailto: soltix@soltix.pl" class="header-mail">soltix@soltix.pl</a>
                </div>
                <span itemprop="telephone">
                    <a href="tel:+48603197858"><span style="color: #ffcb02;">+48 </span> 603 197 858</a>
                </span>
            </div>
        </div>
    </div>
</header>

<main id="app-main">
    <section class="stand-out position-relative" id="stand-out">
        <div class="overflow-hidden">
            <img src="assets/images/standout/figure-1.png" alt="" class="stand-out-image">
            <div class="figure"></div>
        </div>
        <div class="container position-relative p-0">
            <h2 class="fw-bold"><span style="color: #ffcb02;">Co nas</span><br>wyróżnia</h2>
            <div class="info position-relative">
                <div class="overflow-hidden">
                    <img src="./assets/images/standout/figure-2.png" alt="" class="info-image">
                </div>
                <p class="info-title">Jeśli prowadzisz firmę lub jakąkolwiek inną organizację, zarządzasz nią lub
                    którymś z jej obszarów i
                    dostrzegasz potrzebę zmian w sposobie, szybkości i łatwości zarządzania przetwarzanymi danymi
                    <span class="fw-bold">nasza propozycja jest dla Ciebie idealna</span>!</p>
                <div class="d-flex justify-content-between info-blocks">
                    <div class="info-block position-relative">
                        <img src="./assets/images/standout/block-1.png" alt="">
                        <h3>Innowacyjność</h3>
                        <p>Tworzymy oprogramowanie na zamówienie w oparciu o unikalne potrzeby i wymagania zamawiającego
                            oraz charakterystykę firmy. Dla swoich klientów tworzymy programy
                            <strong>"szyte na miarę",</strong> rozwiązujące ich problemy i zaspokajające ich unikalne
                            potrzeby.</p>
                    </div>
                    <div class="info-block position-relative">
                        <img src="./assets/images/standout/block-2.png" alt="">
                        <h3>Efektywność</h3>
                        <p>Dzięki funkcjom oprogramowania tworzonego na zamówienie pomagamy wykorzystać zasoby firmy w
                            najbardziej wydajny sposób. Oprogramowanie tworzone przez nas ma jasny cel:
                            <strong>usprawnienie procesów za pomocą prostej i intuicyjnej obsługi.</strong></p>
                    </div>
                    <div class="info-block position-relative">
                        <img src="./assets/images/standout/block-3.png" alt="">
                        <h3>Kompleksowość</h3>
                        <p>Kompleksowe oprogramowanie, które tworzymy to nie tylko dedykowane rozwiązania wg konkretnych
                            potrzeb klienta, ale także pełna modułowość dająca możliwość późniejszego rozwijania
                            oprogramowania w razie potrzeby zmian lub dalszego rozwoju.</p>
                    </div>
                </div>
            </div>
            <a href="#footer-form" class="info-contact">Zostaw kontakt</a>
        </div>
    </section>
    <section class="activity" id="activity">
        <img src="./assets/images/activity/figure.png" alt="" class="activity-figure">
        <div class="container d-flex">
            <div class="left" id="activity-left">
                <h2 class="header-title"><span>Obszary</span><br>działań</h2>
                <span class="header-subtitle">Rozwiązania <span
                            id="activity-subtitle">Business-to-Business</span></span>
                <p id="activity-text">Oprogramowanie B2B ułatwia wymianę informacji pomiędzy firmami poprzez np.
                    cykliczne wysyłanie
                    korespondencji, automatyczne tworzenie i wysyłanie zleceń, wystawianie i wysyłanie faktur,
                    automatyczne przypominanie o zaległej płatności, itp.</p>
            </div>
            <div class="right">
                <div class="d-flex">
                    <div class="right-block active" title="b2b" onmouseover="changeText(this);" id="b2b">
                        <img src="./assets/images/activity/b2b.png" alt="">
                        <span class="fw-bold">B2B</span>
                        <span class="d-none">Business-to-Business</span>
                        <p class="d-none">Oprogramowanie B2B ułatwia wymianę informacji pomiędzy firmami poprzez np.
                            cykliczne wysyłanie korespondencji, automatyczne tworzenie i wysyłanie zleceń, wystawianie i
                            wysyłanie faktur, automatyczne przypominanie o zaległej płatności, itp.</p>
                    </div>
                    <div class="right-block" title="b2c" onmouseover="changeText(this);">
                        <img src="./assets/images/activity/b2c.png" alt="">
                        <span class="fw-bold">B2C</span>
                        <span class="d-none">Business-to-Consumer</span>
                        <p class="d-none">B2C to wymiana informacji pomiędzy firmą a jej klientem indywidualnym, poprzez
                            np. automatyczną wysyłkę potwierdzenia zakupu, informowanie na bieżąco o stanie zamówienia,
                            wypełnianie listów przewozowych w oparciu o szablon firmy kurierskiej, wysyłanie prośby o
                            informację zwrotną, itd.</p>
                    </div>
                    <div class="right-block" title="crm" onmouseover="changeText(this);">
                        <img src="./assets/images/activity/crm.png" alt="">
                        <span class="fw-bold">CRM</span>
                        <span class="d-none">Customer Relationship Management</span>
                        <p class="d-none">Systemy Customer Relation Management pomagają pozyskać nowych oraz utrzymać
                            stały i efektywny kontakt z posiadanymi już klientami. Ich zadaniem jest gromadzenie i
                            przetwarzanie informacji o kliencie oraz umożliwienie pełnej i kompleksowej jego obsługi,
                            również w sposób automatyczny.</p>
                    </div>
                </div>
                <div class="d-flex block-2">
                    <div class="right-block" title="erp" onmouseover="changeText(this);">
                        <img src="./assets/images/activity/erp.png" alt="">
                        <span class="fw-bold">ERP</span>
                        <span class="d-none">Enterprise Resources Planning</span>
                        <p class="d-none">Systemy ERP pomagają w planowaniu efektywnego wykorzystania zasobów firmy np.
                            w ułożeniu optymalnego harmonogramu pracy dla pracowników, nadzorowania wykorzystania floty
                            samochodów czy skutecznego wykorzystania zasobów produkcyjnych.</p>
                    </div>
                    <div class="right-block" title="oms" onmouseover="changeText(this);">
                        <img src="./assets/images/activity/oms.png" alt="">
                        <span class="fw-bold">OMS</span>
                        <span class="d-none">Order Management System</span>
                        <p class="d-none">Order Management System służą do przyjmowania i przetwarzania zamówień.
                            Ułatwiają realizację zamówień zarówno podczas ich ręcznej obsługi przez pracowników jak i w
                            pełni automatyczną obsługę zamówień składanych np. on-line, tak aby samoczynnie przekazać je
                             do realizacji i wysyłki.</p>
                    </div>
                    <div class="right-block" title="ar" onmouseover="changeText(this);">
                        <img src="./assets/images/activity/ar.png" alt="">
                        <span class="fw-bold">AR</span>
                        <span class="d-none">Augmented Reality</span>
                        <p class="d-none">AR - Technologia Augmented Reality (rozszerzonej rzeczywistości) pozwala
                            prezentować modele 3D na ekranie smartfona lub tabletu (na obrazie uzyskanym z aparatu) w
                            prawdziwej przestrzeni - w rzeczywistych proporcjach i kolorystyce - np.: meble stojące w
                            pokoju na podłodze, wiszące na ścianie lub suficie lampy czy też stojące na tarasie lub na
                            trawie w ogrodzie donice lub pergole.</p>
                    </div>
                    <div class="right-block" title="fa" onmouseover="changeText(this);">
                        <img src="./assets/images/activity/fa.png" alt="">
                        <span class="fw-bold">Financial<br> Analysis</span>
                        <span class="d-none">Financial Analysis</span>
                        <p class="d-none">Systemy wsparcia analiz finansowych pomagają przedsiębiorcom oraz osobom
                            zarządzającym we właściwej interpretacji kondycji finansowej w firmach. Oprócz
                            najzwyklejszego bilansu przychodów i rozchodów pomagają w analizie płynności oraz
                            prezentacji wskaźników finansowych i w prognozach.</p>
                    </div>
                </div>
                <div class="d-flex block-3">
                    <div class="right-block" title="dms" onmouseover="changeText(this);">
                        <img src="./assets/images/activity/dms.png" alt="">
                        <span class="fw-bold">Document<br> Management<br> System</span>
                        <span class="d-none">Document Management System</span>
                        <p class="d-none">Document Management Systems to oprogramowanie służące do tworzenia oraz
                            wymiany dokumentacji w firmie. Dzięki niemu odpowiednie dokumenty, w odpowiednim czasie
                            trafią do odpowiednich osób. DMS zapewnia bezpieczeństwo wymiany dokumentów wewnątrz firmy,
                            a także z podmiotami zewnętrznymi.</p>
                    </div>
                    <div class="right-block" title="ie" onmouseover="changeText(this);">
                        <img src="./assets/images/activity/ie.png" alt="">
                        <span class="fw-bold">Information<br> Exchange</span>
                        <span class="d-none">Information Exchange</span>
                        <p class="d-none">Systemy wymiany informacji to wszelakie systemy stworzone wg sprecyzowanych
                            potrzeb firm do wymiany informacji pomiędzy jej poszczególnymi działami lub obszarami.
                            Dzięki nim osoby zarządzające mogą otrzymywać odpowiednie dane we właściwym czasie.
                            Dzięki temu zarządzanie firmą jest łatwiejsze i efektywniejsze.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="realization" id="realization">
        <div class="overflow-hidden">
            <img src="./assets/images/realization/figure.png" alt="" class="figure">
        </div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col p-0">
                    <div id="carousel" class="carousel slide" data-ride="carousel">
                        <h2 class="header-title"><span>Wybrane</span><br>realizacje</h2>
                        <div class="carousel-inner">
                            <!-- First carousel item -->
                            <div class="carousel-item active">
                                <div class="row align-items-center">
                                    <div class="col-md-5">
                                        <div class="m-0">
                                            <h3 class="slider-title fw-bold">
                                                Aplikacja mobilna Falubaz LIVE
                                            </h3>
                                            <ul class="slider-list">
                                                <li>dedykowana aplikacja klubowa stworzona dla Zielonogórskiego Klubu
                                                    Żużlowego Falubaz, która pozwala kibicom być na bieżąco z
                                                    najważniejszymi informacjami o drużynie
                                                </li>
                                                <li>aplikacja mobilna jest zagregowanym systemem aktualności, które
                                                    dodawane są zarówno przez administratorów z klubu jak i samych
                                                    żużlowców
                                                </li>
                                                <li>aplikacja umożliwia prowadzenie relacji LIVE bezpośrednio z zawodów
                                                    oraz wysyłkę powiadomień push do kibiców
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <img class="d-block img-fluid" src="./assets/images/realization/1.png"
                                             alt="slide">
                                    </div>
                                </div>
                            </div>
                            <!-- End first carousel item -->
                            <!-- Second carousel item -->
                            <div class="carousel-item">
                                <div class="row align-items-center">
                                    <div class="col-md-5">
                                        <div class="m-0">
                                            <h3 class="slider-title fw-bold">
                                                Zarządzanie boxem CrossFit
                                            </h3>
                                            <ul class="slider-list">
                                                <li>tworzenie harmonogramów zajęć i późniejsze zarządzanie nimi</li>
                                                <li>rejestracja i logowanie użytkowników online</li>
                                                <li>zapisy użytkowników na zajęcia poprzez wygodny kalendarz</li>
                                                <li>system zatwierdzania wejść z kart fizycznych za pomocą czytnika w
                                                    recepcji
                                                </li>
                                                <li>zarządzanie płatnościami online</li>
                                                <li>śledzenie statystyk finansowych, wejść/wyjść oraz odwoływanych
                                                    obecności
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <img class="d-block img-fluid" src="./assets/images/realization/2.png"
                                             alt="slide">
                                    </div>
                                </div>
                            </div>
                            <!-- End second carousel item -->
                            <!-- Start carousel item -->
                            <div class="carousel-item">
                                <div class="row align-items-center">
                                    <div class="col-md-5">
                                        <div class="m-0">
                                            <h3 class="slider-title fw-bold">
                                                Program lojalnościowy dla firmy FAST z grupy ROCKWOOL
                                            </h3>
                                            <ul class="slider-list">
                                                <li>zarządzanie profilami użytkowników należących do programu
                                                    lojalnościowego, zarządzanie nagrodami oraz punktacją
                                                </li>
                                                <li>rozbudowany panel użytkownika umożliwiający wprowadzanie punktów,
                                                    rozliczanie karty przedpłaconej, zakup nagród
                                                </li>
                                                <li>rozbudowany algorytm naliczania punktów lojalnościowych</li>
                                                <li>system newslettera oraz powiadomień SMS</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <img class="d-block img-fluid" src="./assets/images/realization/3.png"
                                             alt="slide">
                                    </div>
                                </div>
                            </div>
                            <!-- End carousel item -->
                            <!-- Start carousel item -->
                            <div class="carousel-item">
                                <div class="row align-items-center">
                                    <div class="col-md-5">
                                        <div class="m-0">
                                            <h3 class="slider-title fw-bold">
                                                System Shipper - zarządzanie logistyką transportową
                                            </h3>
                                            <ul class="slider-list">
                                                <li>wprowadzanie zleceń transportowych, określanie danych trasy,
                                                    generowanie listów przewozowych
                                                </li>
                                                <li>zarządzanie bazą klientów, pracowników, przewoźników oraz czarną
                                                    listą
                                                </li>
                                                <li>automatyzacja kontaktów z przewoźnikami i klientami (e-mail
                                                    oraz SMS)
                                                </li>
                                                <li>tracking zlecenia oraz transportu ze statusami oraz automatycznymi
                                                    powiadomieniami
                                                </li>
                                                <li>zarządzanie flotą pojazdów</li>
                                                <li>monitorowanie sprzedaży</li>
                                                <li>kontrolowanie efektywności firmy, generowanie raportów, statystyk
                                                    pracowników
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <img class="d-block img-fluid" src="./assets/images/realization/4.png"
                                             alt="slide">
                                    </div>
                                </div>
                            </div>
                            <!-- End carousel item -->
                            <!-- Start carousel item -->
                            <div class="carousel-item">
                                <div class="row align-items-center">
                                    <div class="col-md-5">
                                        <div class="m-0">
                                            <h3 class="slider-title fw-bold">
                                                Konfigurator ogrodzeń dla firmy PKM Poniatowska
                                            </h3>
                                            <ul class="slider-list">
                                                <li>łatwe projektowanie i wizualizacja ogrodzenia on-line przez klienta
                                                </li>
                                                <li>łatwa skonfiguracja produkt i wysyłka zapytania bezpośrednio do
                                                    producenta
                                                </li>
                                                <li>aplikacja webowa umożliwiająca wybór furtki lub bramy, rozmiarów,
                                                    wzoru oraz dodatkowych dekorów
                                                </li>
                                                <li>CRM, po stronie administratora, umożliwiający zarządzanie i
                                                    przetwarzanie zebranych formularzy, odpowiedzi na zapytania
                                                </li>
                                                <li>zarządzanie przepływem informacji o zamówieniach przez
                                                    administratorów oraz dodawanie do systemu nowych wzorów ogrodzeń
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <img class="d-block img-fluid" src="./assets/images/realization/5.png"
                                             alt="slide">
                                    </div>
                                </div>
                            </div>
                            <!-- End carousel item -->
                            <!-- Start carousel item -->
                            <div class="carousel-item">
                                <div class="row align-items-center">
                                    <div class="col-md-5">
                                        <div class="m-0">
                                            <h3 class="slider-title fw-bold">
                                                Panel rodzica dla szkółki koszykarskiej Minibasket
                                            </h3>
                                            <ul class="slider-list">
                                                <li>panel umożliwiający śledzenie przebiegu zajęć zarówno przez trenerów
                                                    jak i rodziców
                                                </li>
                                                <li>system wystawiania ocen przez trenerów</li>
                                                <li>system komentarzy z przebiegu zajęć i rozwoju dzieci</li>
                                                <li>system szybkiego i automatycznego przepływu informacji pomiędzy
                                                    trenerami a rodzicami (wysłanie wiadomości prywatnej oraz ogólnej do
                                                    wszystkich rodziców)
                                                </li>
                                                <li>system abonamentów i płatności online za członkostwo</li>
                                                <li>system zarządzania kalendarzem zajęć sportowych</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <img class="d-block img-fluid" src="./assets/images/realization/6.png"
                                             alt="slide">
                                    </div>
                                </div>
                            </div>
                            <!-- End carousel item -->
                            <!-- Start carousel item -->
                            <div class="carousel-item">
                                <div class="row align-items-center">
                                    <div class="col-md-5">
                                        <div class="m-0">
                                            <h3 class="slider-title fw-bold">
                                                Widget AR dla sklepów internetowych
                                            </h3>
                                            <ul class="slider-list" style="list-style-type: none;">
                                                <li>Widget AR pozwala prezentować na smartfonie lub tablecie wprowadzone
                                                    obiekty (w formie modeli 3D), w prawdziwej przestrzeni wyświetlanej
                                                    na ekranie, na obrazie uzyskanym z aparatu.
                                                </li>
                                                <li>Widget AR prezentuje na ekranie produkt taki jaki jest w
                                                    rzeczywistości - we właściwych rozmiarach, proporcjach i
                                                    kolorystyce. Klient wybiera interesujący go produkt z menu
                                                    (np. mebel, lampę, donicę, ogrodzenie, itp.) i sprawdza na swoim
                                                    ekranie czy jest dla niego odpowiedni wiedząc, że w pełni
                                                    odzwierciedla on produkt realny.
                                                </li>
                                                <li>Widget AR jest dedykowany przede wszystkim sklepom internetowym</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <img class="d-block img-fluid" src="./assets/images/realization/7.png"
                                             alt="slide">
                                    </div>
                                </div>
                            </div>
                            <!-- End carousel item -->
                        </div>
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carousel" data-bs-slide-to="0"
                                    class="active" aria-current="true" aria-label="Slide 1">
                                <span class="circle"></span>
                            </button>
                            <button type="button" data-bs-target="#carousel" data-bs-slide-to="1"
                                    aria-label="Slide 2">
                                <span class="circle"></span>
                            </button>
                            <button type="button" data-bs-target="#carousel" data-bs-slide-to="2"
                                    aria-label="Slide 3">
                                <span class="circle"></span>
                            </button>
                            <button type="button" data-bs-target="#carousel" data-bs-slide-to="3"
                                    aria-label="Slide 4">
                                <span class="circle"></span>
                            </button>
                            <button type="button" data-bs-target="#carousel" data-bs-slide-to="4"
                                    aria-label="Slide 5">
                                <span class="circle"></span>
                            </button>
                            <button type="button" data-bs-target="#carousel" data-bs-slide-to="5"
                                    aria-label="Slide 6">
                                <span class="circle"></span>
                            </button>
                            <button type="button" data-bs-target="#carousel" data-bs-slide-to="6"
                                    aria-label="Slide 7">
                                <span class="circle"></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<footer id="app-footer" class="position-relative">
    <form action="index.php" id="footer-form" method="post" enctype="multipart/form-data" onsubmit="showMessage()">
        <div class="card">
            <div class="card-header">
                Zostaw dane, skontaktujemy się
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="firstAndLastname">Imię i Nazwisko</label>
                    <input type="text" class="form-control" id="firstAndLastname" name="fullName" placeholder=""
                           minlength="10" required>
                </div>
                <div class="form-group">
                    <label for="email">E-mail</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="" required>
                </div>
                <div class="form-group last-form-group">
                    <label for="phone">Telefon</label>
                    <input type="tel" pattern=".{9}" class="form-control" id="phone" name="phoneNumber" placeholder=""
                           maxlength="9"
                           required>
                </div>
                <div class="d-flex align-items-end justify-content-between">
                    <div class="file-input">
                        <label for="customFile" id="fileLabel">Dodaj plik</label>
                        <input type="file" class="form-control-file" name="myfilename" id="customFile"
                               accept="image/jpeg,image/png,application/pdf">
                    </div>
                    <button type="submit" class="btn btn-primary">Wyślij</button>
                </div>
            </div>
        </div>
    </form>
    <div class="top position-relative">
<!--        <div class="overflow-hidden">-->
<!--            <div id="map"></div>-->
<!--        </div>-->
        <div class="container">
            <img src="./assets/images/logo.svg" alt="Logo" class="">
            <p class="address">
                ul. Gen. Władysława Sikorskiego 19<br>
                65-454 Zielona Góra
            </p>
            <div class="contact d-flex flex-column">
                <a href="mailto: soltix@soltix.pl" class="mail">soltix@soltix.pl</a>
                <span itemprop="telephone">
                    <a href="tel:+48603197858"><span style="color: #ffcb02;">+48 </span> 603 197 858</a>
                </span>
            </div>
            <div class="work-hours">
                <span class="fw-bold">Godziny pracy:</span><br>
                w dni powszednie<br>
                od 8:00 do 16:00
            </div>
        </div>
    </div>
    <div class="bottom">
        <div class="wrapper">
            <nav class="d-flex justify-content-end">
                <a class="nav-link" href="#stand-out">Dlaczego my</a>
                <a class="nav-link" href="#activity">Obszary działań</a>
                <a class="nav-link" href="#realization">Realizacje</a>
                <a class="nav-link" href="#app-footer">Kontakt</a>
            </nav>
        </div>
    </div>
    <div class="copyright">
        <div class="container p-0">
            Copyright © 2021 SOLTIX | Wszystkie prawa zastrzeżone.
        </div>
    </div>
</footer>

<!-- JQuery -->
<script src="./assets/scripts/jquery-3.6.0.min.js"></script>
<!-- Bootstrap JS -->
<script src="./assets/scripts/popper.min.js"></script>
<script src="./assets/scripts/bootstrap.min.js"></script>
<!-- Google Map -->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYJmONHsDnzAEEe6dj_XdwSHfzfOc_OQM&callback=initMap&libraries=&v=weekly"-->
<!--        async></script>-->
<!-- Custom Scripts -->
<script src="./assets/scripts/main.js"></script>

</body>
</html>
